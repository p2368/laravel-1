@extends('layout')

@section('title', 'Selamat Datang')

@section('content')
    <h1>SELAMAT DATANG <?= strtoupper($request['firstname']) ?> <?= strtoupper($request['lastname']) ?></h1>
    <h2>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h2>
@endsection