@extends('layout')

@section('title', 'Buat Account Baru!')

@section('content')
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name:</label><br>
        <br>
        <input type="text" name="firstname" id="firstname" required><br>
        <br>
        <label for="lastname">Last name:</label><br>
        <br>
        <input type="text" name="lastname" id="lastname" required><br>
        <br>
        <label for="gender">Gender:</label><br>
        <br>
        <input type="radio" name="gender" value="0" required> Male<br>
        <input type="radio" name="gender" value="1"> Female<br>
        <input type="radio" name="gender" value="2"> Other<br>
        <br>
        <label for="nationality">Nationality:</label><br>
        <br>
        <select name="nationality" id="nationality" required>
            <option value="0">Indonesian</option>
            <option value="1">Chinese</option>
            <option value="2">Malaysian</option>
            <option value="3">American</option>
            <option value="4">British</option>
        </select><br>
        <br>
        <label for="language">Language Spoken:</label><br>
        <br>
        <input type="checkbox" name="language" value="0" required> Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="1"> English<br>
        <input type="checkbox" name="language" value="2"> Other<br>
        <br>
        <label for="bio">Bio:</label><br>
        <br>
        <textarea name="bio" id="bio" cols="30" rows="10" required></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection